package com.dexciuq.todoapp.model

data class TodoList(
    val id: Long,
    var name: String
)

data class TodoItem(
    val id: Long,
    var name: String,
    var completed: Boolean
)