 package com.dexciuq.todoapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.todoapp.adapter.TodoListAdapter
import com.dexciuq.todoapp.databinding.ActivityMainBinding
import com.dexciuq.todoapp.databinding.DialogAddBinding
import com.dexciuq.todoapp.db.DatabaseHelper
import com.dexciuq.todoapp.model.TodoList


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var todoListAdapter: TodoListAdapter
    private val database: DatabaseHelper by lazy { DatabaseHelper(this) }
    private val todoLists: MutableList<TodoList> by lazy { database.getAllLists() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setUpRecyclerView(binding.recyclerView)

        binding.fab.setOnClickListener { showAddListDialog() }
    }

    private fun setUpRecyclerView(view: RecyclerView) {
        todoListAdapter = TodoListAdapter(
            context = this,
            values = todoLists,
            onItemEdit = { database.updateList(it) },
            onItemDelete = { database.deleteList(it) }
        )
        view.adapter = todoListAdapter
        view.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        R.id.settings -> {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun showAddListDialog() {
        val binding = DialogAddBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.textInputLayout.setHint(R.string.hint_list_name)
        binding.cancelButton.setOnClickListener {
            dialog.dismiss()
        }

        binding.addButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "List name cannot be empty"
            else {
                val todoList = database.addList(name)
                todoLists.add(todoList)
                todoListAdapter.notifyItemInserted(todoLists.lastIndex)
                dialog.dismiss()
            }
        }
        dialog.show()
    }
}