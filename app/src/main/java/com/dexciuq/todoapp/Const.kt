package com.dexciuq.todoapp

const val SHARED_PREFERENCES_NAME = "ColorSettings"

const val COMPLETED_COLOR = "completedColor"
const val NOT_COMPLETED_COLOR = "notCompletedColor"

const val LIST_ID = "listId"
const val LIST_NAME = "listName"