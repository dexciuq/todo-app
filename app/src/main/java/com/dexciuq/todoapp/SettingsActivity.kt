package com.dexciuq.todoapp

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.dexciuq.todoapp.databinding.ActivitySettingsBinding

class SettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySettingsBinding
    private lateinit var prefs: SharedPreferences

    private var completed: Int = 0
    private var notCompleted: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        completed = prefs.getInt(COMPLETED_COLOR, getColor(R.color.green))
        notCompleted = prefs.getInt(NOT_COMPLETED_COLOR, getColor(R.color.red))

        binding.completedGroup.setOnCheckedChangeListener { _, id ->
            completed = when (id) {
                R.id.brown -> getColor(R.color.brown)
                R.id.green -> getColor(R.color.green)
                R.id.blue -> getColor(R.color.blue)
                else -> throw IllegalArgumentException("Unknown color")
            }
        }

        binding.notCompletedGroup.setOnCheckedChangeListener { _, id ->
            notCompleted = when (id) {
                R.id.red -> getColor(R.color.red)
                R.id.pink -> getColor(R.color.pink)
                R.id.violet -> getColor(R.color.violet)
                else -> throw IllegalArgumentException("Unknown color")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> { finish() }
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        prefs.edit().run {
            putInt(COMPLETED_COLOR, completed)
            putInt(NOT_COMPLETED_COLOR, notCompleted)
            apply()
        }
    }
}