package com.dexciuq.todoapp.adapter

import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.todoapp.DetailActivity
import com.dexciuq.todoapp.LIST_ID
import com.dexciuq.todoapp.LIST_NAME
import com.dexciuq.todoapp.R
import com.dexciuq.todoapp.databinding.DialogUpdateBinding
import com.dexciuq.todoapp.databinding.FragmentListBinding
import com.dexciuq.todoapp.model.TodoList

class TodoListAdapter(
    private val context: Context,
    private val values: MutableList<TodoList>,
    private val onItemEdit: (todoList: TodoList) -> Unit,
    private val onItemDelete: (id: Long) -> Unit
): RecyclerView.Adapter<TodoListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentListBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.name.text = item.name
        holder.modify.setOnClickListener { showPopupMenu(holder.itemView, position, item) }
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(LIST_ID, item.id)
            intent.putExtra(LIST_NAME, item.name)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = values.size

    private fun showPopupMenu(view: View, position: Int, todoList: TodoList) {
        PopupMenu(context, view, Gravity.END).run {
            inflate(R.menu.menu_list_popup)
            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_edit -> showUpdateListDialog(todoList, position)
                    R.id.action_delete -> {
                        values.remove(todoList)
                        onItemDelete(todoList.id)
                        notifyItemRemoved(position)
                    }
                }
                true
            }
            show()
        }
    }

    private fun showUpdateListDialog(todoList: TodoList, position: Int) {
        val binding = DialogUpdateBinding.inflate(LayoutInflater.from(context))
        val builder = AlertDialog.Builder(context).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.textInputLayout.setHint(R.string.hint_new_list_name)
        binding.editText.append(todoList.name)

        binding.cancelButton.setOnClickListener { dialog.dismiss() }
        binding.updateButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "List name cannot be empty"
            else {
                todoList.name = name
                onItemEdit(todoList)
                notifyItemChanged(position)
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    inner class ViewHolder(binding: FragmentListBinding) : RecyclerView.ViewHolder(binding.root) {
        val name = binding.name
        val modify = binding.modify
    }
}