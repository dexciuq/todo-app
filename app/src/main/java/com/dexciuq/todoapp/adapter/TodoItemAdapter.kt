package com.dexciuq.todoapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.todoapp.R
import com.dexciuq.todoapp.databinding.DialogUpdateBinding
import com.dexciuq.todoapp.databinding.FragmentItemBinding
import com.dexciuq.todoapp.model.TodoItem

class TodoItemAdapter(
    private val context: Context,
    private val values: MutableList<TodoItem>,
    private val completed: Int,
    private val notCompleted: Int,
    private val onItemEdit: (todoItem: TodoItem) -> Unit,
    private val onItemDelete: (id: Long) -> Unit
): RecyclerView.Adapter<TodoItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentItemBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.name.text = item.name
        if (item.completed) {
            holder.name.setTextColor(completed)
            holder.checkbox.isChecked = true
        }
        else {
            holder.name.setTextColor(notCompleted)
            holder.checkbox.isChecked = false
        }


        holder.checkbox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                holder.name.setTextColor(completed)
                item.completed = true
            }
            else {
                holder.name.setTextColor(notCompleted)
                item.completed = false
            }
            onItemEdit(item)
        }

        holder.edit.setOnClickListener {
            showUpdateItemDialog(item, position)
        }

        holder.delete.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("Are you sure?")
                .setCancelable(false)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    onItemDelete(item.id)
                    values.remove(item)
                    notifyItemRemoved(position)
                }
                .create()
                .show()
        }
    }

    private fun showUpdateItemDialog(todoItem: TodoItem, position: Int) {
        val binding = DialogUpdateBinding.inflate(LayoutInflater.from(context))
        val builder = AlertDialog.Builder(context).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.textInputLayout.setHint(R.string.hint_new_item_name)
        binding.editText.append(todoItem.name)

        binding.cancelButton.setOnClickListener { dialog.dismiss() }
        binding.updateButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "Item name cannot be empty"
            else {
                todoItem.name = name
                onItemEdit(todoItem)
                notifyItemChanged(position)
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val name = binding.name
        val edit = binding.actionEdit
        val delete = binding.actionDelete
        val checkbox = binding.checkbox
    }
}