package com.dexciuq.todoapp

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.todoapp.adapter.TodoItemAdapter
import com.dexciuq.todoapp.databinding.ActivityDetailBinding
import com.dexciuq.todoapp.databinding.DialogAddBinding
import com.dexciuq.todoapp.db.DatabaseHelper
import com.dexciuq.todoapp.model.TodoItem
import com.google.android.material.appbar.MaterialToolbar

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var prefs: SharedPreferences
    private lateinit var todoItemAdapter: TodoItemAdapter
    private lateinit var todoItems: MutableList<TodoItem>
    private val database: DatabaseHelper by lazy { DatabaseHelper(this) }
    private var listId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        intent.extras?.let {
            listId = it.getLong(LIST_ID)
            todoItems = database.getAllItems(it.getLong(LIST_ID))
            binding.toolbar.title = it.getString(LIST_NAME)
        }

        setContentView(binding.root)
        setUpSupportActionBar(binding.toolbar)
        setUpRecyclerView(binding.recyclerView)

        binding.fab.setOnClickListener { showAddItemDialog() }
    }

    private fun setUpRecyclerView(view: RecyclerView) {
        todoItemAdapter = TodoItemAdapter(
            context = this,
            values = todoItems,
            completed = prefs.getInt(COMPLETED_COLOR, getColor(R.color.green)),
            notCompleted = prefs.getInt(NOT_COMPLETED_COLOR, getColor(R.color.red)),
            onItemEdit = { database.updateItem(it) },
            onItemDelete = { database.deleteItem(it) }
        )
        view.adapter = todoItemAdapter
        view.layoutManager = LinearLayoutManager(this)
    }

    private fun setUpSupportActionBar(toolbar: MaterialToolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> { finish() }
        }
        return true
    }

    private fun showAddItemDialog() {
        val binding = DialogAddBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this).apply { setView(binding.root) }
        val dialog = builder.create()

        binding.textInputLayout.setHint(R.string.hint_item_name)

        binding.cancelButton.setOnClickListener { dialog.dismiss() }
        binding.addButton.setOnClickListener {
            val editText = binding.editText
            val name = editText.text.toString()

            if (name.isBlank()) editText.error = "Item name cannot be empty"
            else {
                val todoItem = database.addItem(name, listId)
                todoItems.add(todoItem)
                todoItemAdapter.notifyItemInserted(todoItems.lastIndex)
                dialog.dismiss()
            }
        }
        dialog.show()
    }
}